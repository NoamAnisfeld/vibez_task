TASK: <b>Change state of Row without refreshing all table on Edit action</b>
<br></br>
Steps:</br>

- Fork project
- Run project on localhost:3000( login: nat.salyaeva@gmail.com password: 123456 )
- perform task
- check “Add Product” functionality still working
- Put your contacts here:
    Noam Anisfeld
    Email: NoamAnisfeld@gmail.com
    Tel: 050-5511266
    Linkedin: https://www.linkedin.com/in/noam-anisfeld/
- Create Merge request </br>

</br>
Good luck! =)
