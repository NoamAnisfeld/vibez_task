// Chakra imports
import { Icon, Flex, Avatar, Text, useColorModeValue } from "@chakra-ui/react";
// Assets
import React from "react";

export function UserCell({ user }) {
  const textColor = useColorModeValue("secondaryGray.900", "white");
  const name = user?.first_name + " " + user?.last_name;

  return (
    <Flex>
      <Flex direction={"column"} justifyContent={"center"}>
        <Avatar
          h="42px"
          w="42px"
          color="white"
          bg="navy"
          src={user?.avatar}
          me="14px"
          name={name}
        />
      </Flex>

      <Flex direction={"column"} justifyContent={"center"}>
        <Text me="10px" color={textColor} fontSize="sm" fontWeight="500">
          {name.toUpperCase()}
        </Text>
        <Text me="10px" color={textColor} fontSize="sm">
          {user?.email}
        </Text>
      </Flex>
    </Flex>
  );
}
