export const columnsDataProducts = [
  {
    Header: "NAME",
    accessor: "name",
  },
  {
    Header: "STATUS",
    accessor: "status",
  },
  {
    Header: "TYPE",
    accessor: "product_type",
  },
  {
    Header: "DESCRIPTION",
    accessor: "description",
  },
  {
    Header: "PRICE",
    accessor: "price",
  },
  {
    Header: "AMOUNT",
    accessor: "dob",
  },
  {
    Header: "RESERVED",
    accessor: "reserved",
  },
  {
    Header: "SOLD",
    accessor: "sold",
  },
  {
    Header: "",
    accessor: "test",
  },
];
