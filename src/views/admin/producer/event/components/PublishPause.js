/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";

// Chakra imports
import {
  Flex,
  useColorModeValue,
  FormControl,
  Button,
  Text,
  FormLabel,
  Switch,
} from "@chakra-ui/react";
import Card from "components/card/Card.js";

// Custom components

export default function PublishPause({ event_path, user, setEvent, event }) {
  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
  const [errorMsg, setErrorMsg] = useState("");
  const [inProgress, setInProgress] = useState(false);

  const showMessage = (msg) => {
    setErrorMsg(msg);
    setInterval(() => {
      setErrorMsg("");
    }, 2000);
  };

  const togglePause = () => {
    let data = { paused: !event?.paused };
    handleSave(data);
  };

  const togglePublish = () => {
    let data = { published: !event?.published };
    handleSave(data);
  };

  const handleSave = async (data) => {
    if (inProgress) return;
    if (!user) return;
    setErrorMsg("");
    setInProgress(true);
    const response = await fetch(event_path, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: user,
      },
      body: JSON.stringify({
        event: data,
      }),
    });
    const json = await response.json();
    console.log("JSON: ", JSON.stringify(json));

    if (!response.ok) {
      showMessage(json.error.message);
    }
    if (response.ok) {
      console.log("RES: ", JSON.stringify(json));
      setEvent(json.event);
    }
    setInProgress(false);
    return;
  };

  // Chakra Color Mode
  return (
    <Card mb="20px">
      <Text
        fontSize="md"
        color={textColorPrimary}
        fontWeight="bold"
        marginInlineStart={"10px"}
        mb={"20px"}
        alignSelf={"center"}
      >
        Event number: {event?.number}
      </Text>
      <Flex
        alignContent={"center"}
        textAlign="center"
        justifyContent={"center"}
      >
        <FormControl w="200px">
          <Flex justifyContent={"space-between"}>
            <FormLabel htmlFor="isChecked">Publish:</FormLabel>
            <Switch
              onChange={() => {
                if (!inProgress) togglePublish();
              }}
              size="lg"
              isChecked={event?.published}
            />
          </Flex>
          <Flex justifyContent={"space-between"}>
            <FormLabel htmlFor="isChecked">Pause:</FormLabel>
            <Switch
              onChange={() => {
                if (!inProgress) togglePause();
              }}
              size="lg"
              isChecked={event?.paused}
            />
          </Flex>
        </FormControl>
      </Flex>
      {errorMsg && <Text color="red">{errorMsg}</Text>}
    </Card>
  );
}
